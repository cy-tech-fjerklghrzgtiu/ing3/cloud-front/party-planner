import { Component, Input } from '@angular/core';

export interface ColumnLink {
  name: string;
  link: string;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {
  @Input()
  columns: ColumnLink[] = [{ name: "ID", link: "id" }, { name: "Name", link: "name" }, { name: "Weight", link: "weight" }, { name: "Symbol", link: "symbol" }];
  @Input()
  rows: any[] = [
    { id: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  ]
}
