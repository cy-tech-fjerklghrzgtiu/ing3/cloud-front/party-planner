import { Component } from '@angular/core';
import { Router, Routes } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  routes: { path: string, title: string, check: Function | undefined }[] = [
    { path: '/', title: 'Home', check: undefined },
    { path: '/events', title: 'Events', check: undefined },
    { path: '/users', title: 'Users', check: undefined },
    { path: '/signup', title: 'Sign-up', check: undefined },
    { path: '/signin', title: 'Sign-in', check: undefined },
    { path: '/profile', title: 'Profile', check: this.authService.isAuth, },
  ];

  constructor(private router: Router, private authService: AuthService) { }

}
