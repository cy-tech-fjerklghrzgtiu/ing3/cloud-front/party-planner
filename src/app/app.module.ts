import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent as HomeComponent } from './pages/home/home.component';
import { FooterComponent } from './template/footer/footer.component';
import { MenuComponent } from './template/menu/menu.component';
import { EventsComponent as EventsComponent } from './pages/events/events.component';
import { SingupComponent as SingupComponent } from './pages/singup/singup.component';
import { ProfilComponent as ProfileComponent } from './pages/profil/profil.component';
import { UsersComponent as UsersComponent } from './pages/users/users.component';
import { SigninComponent as SigninComponent } from './pages/signin/signin.component';
import { LegalsComponent } from './pages/legals/legals.component';
import { TableComponent } from './components/table/table.component';
import { ContactComponent as ContactComponent } from './pages/contact/contact.component';
import { FormsModule } from '@angular/forms';
import { ErrorComponent } from './pages/error/error.component';
import { EventComponent } from './pages/event/event.component';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getAuth, provideAuth } from '@angular/fire/auth';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { getDatabase, provideDatabase } from '@angular/fire/database';

@NgModule({
  declarations: [
    MenuComponent,
    FooterComponent,

    AppComponent,
    HomeComponent,

    SingupComponent,
    SigninComponent,
    UsersComponent,
    ProfileComponent,

    LegalsComponent,
    ContactComponent,

    ErrorComponent,
    EventsComponent,
    EventComponent,

    TableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    provideFirebaseApp(() => initializeApp({"projectId":"party-planner-405114","appId":"1:192162620395:web:dfee0fadb78ec8d275afae","storageBucket":"party-planner-405114.appspot.com","apiKey":"AIzaSyBjEbPb5O_oMmLzXQcHqrC-2uHnOUStefI","authDomain":"party-planner-405114.firebaseapp.com","messagingSenderId":"192162620395"})),
    provideAuth(() => getAuth()),
    provideFirestore(() => getFirestore()),
    provideDatabase(() => getDatabase())
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
