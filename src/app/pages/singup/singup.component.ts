import { Component } from '@angular/core';
import { Status } from 'src/app/models/user';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent {

  form: {
    username: string,
    firstname: string,
    lastname: string,
    email: string,
    password: string,
    passwordConfirm: string
  } = {
      username: '',
      firstname: '',
      lastname: '',
      email: '',
      password: '',
      passwordConfirm: ''
    }

  notMatchingPassword: boolean = false;
  usernameAlreadyExists: boolean = false;
  usernameEmpty: boolean = false;

  firstnameEmpty: boolean = false;
  lastnameEmpty: boolean = false;

  emailEmpty: boolean = false;
  emailInvalid: boolean = false;
  emailAlreadyExists: boolean = false;

  passwordEmpty: boolean = false;
  passwordConfirmEmpty: boolean = false;

  constructor(private usersService: UsersService) { }

  signup() {
    if (this.form.password !== this.form.passwordConfirm) {
      this.notMatchingPassword = true;
    } else {
      this.notMatchingPassword = false;
    }

    if (this.form.username === '') {
      this.usernameEmpty = true;
    } else {
      this.usernameEmpty = false;
    }

    if (this.form.firstname === '') {
      this.firstnameEmpty = true;
    } else {
      this.firstnameEmpty = false;
    }

    if (this.form.lastname === '') {
      this.lastnameEmpty = true;
    } else {
      this.lastnameEmpty = false;
    }

    if (this.form.email === '') {
      this.emailEmpty = true;
    } else {
      this.emailEmpty = false;
    }

    if (this.form.password === '') {
      this.passwordEmpty = true;
    } else {
      this.passwordEmpty = false;
    }

    if (this.form.passwordConfirm === '') {
      this.passwordConfirmEmpty = true;
    } else {
      this.passwordConfirmEmpty = false;
    }

    if (!this.usernameEmpty && !this.firstnameEmpty && !this.lastnameEmpty && !this.emailEmpty && !this.passwordEmpty && !this.passwordConfirmEmpty && !this.notMatchingPassword) {
      this.usersService.addUser({
        uuid: crypto.randomUUID(),
        username: this.form.username,
        firstname: this.form.firstname,
        lastname: this.form.lastname,
        email: this.form.email,
        role: Status.User,
        password: this.form.password
      });
    }
  }
}
