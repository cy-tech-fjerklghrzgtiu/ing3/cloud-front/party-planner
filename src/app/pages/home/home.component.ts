import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  moustache: string = 'Affiche moi des moustache steuplé {{ !';
  listeMoustaches: Array<string> = ['Moustache', 'Favoris'];
  autreListeMoustaches: string[] = [];

  peupleMoustaches() {
    console.log('Peuple moustache appelé');
  }
}
