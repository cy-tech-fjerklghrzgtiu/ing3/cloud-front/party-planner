import { Component } from '@angular/core';
import { ColumnLink } from 'src/app/components/table/table.component';
import { Status, User } from 'src/app/models/user';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {
  users: Set<User> = new Set<User>();

  constructor(private userService: UsersService) {
    this.users = userService.getUsers();
  }
}
