import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Event } from 'src/app/models/event';
import { EventsService } from 'src/app/services/events.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent {

  event: Event | undefined;

  constructor(private eventsS: EventsService, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.event = eventsS.getEvent(params["uuid"])
    });
  }

}
