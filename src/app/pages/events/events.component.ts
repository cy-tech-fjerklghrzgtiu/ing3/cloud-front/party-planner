import { Component } from '@angular/core';
import { ColumnLink } from 'src/app/components/table/table.component';
import { Event } from 'src/app/models/event';
import { EventsService } from 'src/app/services/events.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent {
  events: Set<Event> = new Set<Event>();

  constructor(private eventsS: EventsService) {
    this.events = eventsS.getEvents();
  }
}
