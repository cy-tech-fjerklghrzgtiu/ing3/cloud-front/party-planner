import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'orga-root',
  templateUrl: './orga.component.html',
  styleUrls: ['./orga.component.css']
})
export class OrgaComponent {
  title = 'admin-portal';
  soustitre: string = "Admin Portal";
}
