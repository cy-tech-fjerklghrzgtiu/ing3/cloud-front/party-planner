import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrgaComponent } from "./orga.component";
import { AppRoutingModule } from '../app-routing.module';
import { TestComponent } from './test/test.component';
import { OrgaRoutingModule } from './orga-routing.module';


@NgModule({
  declarations: [
    OrgaComponent,
    TestComponent
  ],
  imports: [
    CommonModule,
    OrgaRoutingModule
  ]
})
export class OrgaModule { }
