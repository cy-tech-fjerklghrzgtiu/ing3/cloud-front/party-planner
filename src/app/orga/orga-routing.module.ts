import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrgaModule } from './orga.module';
import { TestComponent } from './test/test.component';
import { OrgaComponent } from './orga.component';

const routes: Routes = [
  {
    path: 'orga',
    component: OrgaComponent,
    children: [
      // { path: '', component: OrgaComponent },
      { path: 'test', component: TestComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrgaRoutingModule {

}
