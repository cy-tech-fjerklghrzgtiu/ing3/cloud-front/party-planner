import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProfilComponent } from './pages/profil/profil.component';
import { UsersComponent } from './pages/users/users.component';
import { SigninComponent } from './pages/signin/signin.component';
import { SingupComponent } from './pages/singup/singup.component';
import { EventsComponent } from './pages/events/events.component';
import { LegalsComponent } from './pages/legals/legals.component';
import { ContactComponent } from './pages/contact/contact.component';
import { ErrorComponent } from './pages/error/error.component';
import { EventComponent } from './pages/event/event.component';
import { adminGuard } from './guards/admin.guard';

const routes: Routes = [
  { title: 'Accueil', path: '', component: HomeComponent },
  { title: 'Profile', path: 'profile', component: ProfilComponent },
  { title: 'Users', path: 'users', component: UsersComponent },
  { title: 'Sign-in', path: 'signin', component: SigninComponent },
  { title: 'Sign-up', path: 'signup', component: SingupComponent },
  { title: 'Events', path: 'events', component: EventsComponent },
  { title: 'Event', path: 'events/:uuid', component: EventComponent },
  { title: 'Legals', path: 'legals', component: LegalsComponent },
  { title: 'Contact', path: 'contact', component: ContactComponent },
  {
    title: 'Orga',
    loadChildren: () => import('./orga/orga.module').then(m => m.OrgaModule),
    path: 'orga',
    canActivate: [adminGuard]
  },
  { title: 'Error', path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
