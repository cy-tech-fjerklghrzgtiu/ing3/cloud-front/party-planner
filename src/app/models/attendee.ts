export interface Attendee {
    user_uuid: string | undefined;
    firstname: string;
    lastname: string;
    external: boolean;
    sam: boolean;
}
