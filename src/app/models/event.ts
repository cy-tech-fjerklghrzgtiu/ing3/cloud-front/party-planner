import { Attendee } from "./attendee";
import { User } from "./user";

export interface Event {
    uuid: number;
    title: string;
    date: number;
    description: string;
    pictures: Map<string, string>;
    location: string;
    capacity: number;
    type: string;
    attendees: Attendee[];
}
