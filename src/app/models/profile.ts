import { User } from "./user";

export interface UserProfile {
    user: User;
}
