export enum Status {
    Anonymous = 'anonymous',
    User = 'user',
    Manager = 'manager',
    Admin = 'admin',

}

export interface User {
    uuid: string;
    username: string
    firstname: string;
    lastname: string;
    email: string;
    role: Status;
    password: string | undefined;
}
