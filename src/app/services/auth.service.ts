import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserProfile } from '../models/profile';
import { User } from '../models/user';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authInfo: { username: string, password: string } = { username: "", password: "" };
  profil: UserProfile | undefined;

  constructor(private http: HttpClient, private router: Router, private usersService: UsersService) {

  }

  auth() {
    // this.http.get(`assets/data/users/${this.authInfo?.username}@${this.authInfo?.password}.json`).subscribe(
    //   {
    //     next: (user) => {
    //       console.log(user)
    //       this.profil = {
    //         user: user as User
    //       };
    //       this.router.navigateByUrl("/profil")
    //     },
    //     error: (err) => {
    //       console.log(err)
    //     }
    //   }
    // )
    let user = this.usersService.getUserByUsername(this.authInfo!.username);
    if (user && user.password === this.authInfo!.password) {
      this.profil = {
        user: user
      };
      this.router.navigateByUrl("/profile")
    }
  }

  isAuth(): boolean {
    return !!this.profil;
  }
}
