import { Injectable } from '@angular/core';
import { Event } from '../models/event';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  events: Set<Event> = new Set<Event>([
    {
      uuid: 0,
      title: 'Back To School',
      description: `
      Join us for a fun-filled day as we celebrate the return to class!
      We'll have games, music, and plenty of food and drinks to keep you energized throughout the day. Whether you're a student, teacher, or just someone who loves learning, this event is for you.
      Don't miss out on the chance to connect with others and kick off the new school year in style. See you there!
      `,
      pictures: new Map<string, string>([
        ["School Bus", "/assets/events/0/poster.jpg"],
      ]),
      type: 'Party',
      location: 'Gymnase Saint-James',
      date: Date.parse('2021-09-01T21:00:00'),
      capacity: 300,
      attendees: [
        {
          user_uuid: undefined,
          firstname: 'Quentin',
          lastname: 'Garat',
          external: false,
          sam: false,
        },
        {
          user_uuid: undefined,
          firstname: 'Samuel',
          lastname: 'Rodrigues',
          external: false,
          sam: false,
        },
      ]
    },
    {
      uuid: 1,
      title: 'Halloween Party',
      description: `
      Join us for a spooktacular school Halloween party!
      Get ready for a night of frights, delights, and fun.
      We'll have a costume contest, so come dressed in your Halloween best.
      There will be games, music, and a haunted house that will send chills down your spine.
      Don't forget to try the delicious Halloween-themed treats and drinks.
      This is a night of fun you won't want to miss.
      See you there, if you dare!
      `,
      pictures: new Map<string, string>([
        ["School Bus", "/assets/events/1/poster.jpg"],
      ]),
      type: 'Party',
      location: 'Gymnase Saint-James',
      date: Date.parse('2021-10-10T21:00:00'),
      capacity: 300,
      attendees: []
    },
    {
      uuid: 2,
      title: 'Christmas Ball',
      description: `
      Join us for a night of holiday cheer!
      We'll have music, food, and drinks to keep you dancing all night long.
      Don't forget to bring your ugly sweater and get ready to party.
      This is a night you won't want to miss.
      See you there!
      `,
      pictures: new Map<string, string>([]),
      type: 'Ball',
      location: 'Chatêau de Versailles',
      date: Date.parse('2021-12-01T21:00:00'),
      capacity: 300,
      attendees: []
    }
  ]);

  constructor() { }

  getEvents(): Set<Event> {
    return this.events;
  }

  getEvent(id: number): Event | undefined {
    let foundEvent: Event | undefined;
    this.events.forEach(event => {
      if (event.uuid == id) {
        foundEvent = event;
      }
    });

    return foundEvent;
  }

  addEvent(event: Event): void {
    this.events.add(event);
  }
}
