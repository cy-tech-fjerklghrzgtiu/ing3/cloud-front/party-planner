import { Injectable } from '@angular/core';
import { Status, User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  users: Set<User> = new Set<User>([
    {
      uuid: '1',
      username: 'test',
      firstname: 'John',
      lastname: 'Doe',
      email: 'test@mail.com',
      role: Status.Admin,
      password: 'test'
    }
  ]);

  constructor() { }

  getUsers(): Set<User> {
    return this.users;
  }

  getUserByUUID(uuid: string): User | undefined {
    let foundUser: User | undefined;
    this.users.forEach(user => {
      if (user.uuid === uuid) {
        foundUser = user;
      }
    });

    return foundUser;
  }

  getUserByUsername(username: string): User | undefined {
    let foundUser: User | undefined;
    this.users.forEach(user => {
      if (user.username === username) {
        foundUser = user;
      }
    });

    return foundUser;
  }

  addUser(user: User): void {
    this.users.add(user);
  }


}
